
function triParInsertion(tableau){
 
//pacours de l'ensemble du tableau                
for (let i = 1; i < tableau.length; i++) {
  const tabElement = tableau[i];
 var j = i-1

 while (j >= 0 && tableau[j] > tabElement) {
    // déplacement du nombre
    tableau[j+1] = tableau[j]
    j=j-1
  }

   //Insère la valeur temporaire à la position 
  //correcte dans la partie triée.
  tableau[j+1] = tabElement

}

return tableau
}


function triShaker(arr) {
	let start = 0, end = arr.length, swapped = true;

	while (swapped) {
		swapped = false;
		for (let i = start; i < end - 1; i++) {
			if (arr[i] > arr[i+1]) {
				let temp = arr[i];
				arr[i] = arr[i+1];
				arr[i+1] = temp;
				swapped = true;
			}
		}

		end--;
		if (!swapped)
			break;
    
		swapped = false;
		for (let i = end - 1; i > start; i--) {
			if (arr[i - 1] > arr[i]) {
				let temp = arr[i];
				arr[i] = arr[i - 1];
				arr[i - 1] = temp;
				swapped = true;
			}
		}

		start++;
	}

	return arr;
}
   

function triBulle(tableau){
      for (let i = 0; i < tableau.length; i++) {
        for (let j = 0; j < tableau.length; j++) {
           if(tableau[j]>tableau[j+1]){
               var  element =tableau[j]
               tableau[j]=tableau[j+1]
               tableau[j+1]=element
           }
          }   
    }
    return tableau
}

function triParSelection(tableau){
    for (let i = 0; i < tableau.length; i++) {
     var indexElemtMinimum =i
     for (let j = i+1; j < tableau.length; j++) {
        if(tableau[j] < tableau[indexElemtMinimum]){
           
            indexElemtMinimum = j; 
           }
       
     }
     var tabElement = tableau[i];
     tableau[i] = tableau[indexElemtMinimum];
     tableau[indexElemtMinimum] = tabElement;
      
    }
      return tableau;
}
function swap(items, leftIndex, rightIndex){
    var temp = items[leftIndex];
    items[leftIndex] = items[rightIndex];
    items[rightIndex] = temp;
}
function partition(items, left, right) {
    var pivot   = items[Math.floor((right + left) / 2)], //middle element
        i       = left, //left pointer
        j       = right; //right pointer
    while (i <= j) {
        while (items[i] < pivot) {
            i++;
        }
        while (items[j] > pivot) {
            j--;
        }
        if (i <= j) {
            swap(items, i, j); //sawpping two elements
            i++;
            j--;
        }
    }
    return i;
}

function triRapide(items, left, right) {
    var index;
    if (items.length > 1) {
        index = partition(items, left, right); //index returned from partition
        if (left < index - 1) { //more elements on the left side of the pivot
            triRapide(items, left, index - 1);
        }
        if (index < right) { //more elements on the right side of the pivot
            triRapide(items, index, right);
        }
    }
    return items;
}


function fusion(left, right){
  
    var tab = [], l = 0, r = 0;
    while (l < left.length && r < right.length){
        if (left[l] < right[r]){
            tab.push(left[l++]);
        } else {
            tab.push(right[r++]);
        }
    }
    return tab.concat(left.slice(l)).concat(right.slice(r));
}
function triFusion(tab){
    if (tab.length < 2) {
        return tab;
    }
    var mid = Math.floor(tab.length / 2),
        right = tab.slice(mid),
        left = tab.slice(0, mid),
        p = fusion(triFusion(left), triFusion(right));
    
    p.unshift(0, tab.length);
    tab.splice.apply(tab, p);
    return tab;
}
function echanger (arbre, a, b) {
    var temp = arbre[a];
    arbre[a] = arbre[b];
    arbre[b] = temp;
}


function tamiser (arbre, noeud, n) {
    var k = noeud;
    var j = 2 * k;
    while (j <= n) {
        if ((j < n) && (arbre[j] < arbre[j + 1]))
            j++;
        if (arbre[k] < arbre[j]) {
            echanger(arbre, k, j);
            k = j;
            j = 2 * k;
        } else
            break;
    }
}

function triParTas(arbre) {
    for (var i = arbre.length >> 1; i >= 0; i--)
        tamiser(arbre, i, arbre.length - 1);
    for (var i = arbre.length - 1; i >= 1; i--) {
        echanger(arbre, i, 0);
        tamiser(arbre, 0, i - 1);
    }
    return arbre
}


function triGnome(arr) 
{
    function Deplacer(i) 
  {
        for( ; i > 0 && arr[i-1] > arr[i]; i--)
        {
            var t = arr[i];
            arr[i] = arr[i-1];
            arr[i-1] = t;
        }
    }
    for (var i = 1; i < arr.length; i++) 
    {
        if (arr[i-1] > arr[i]) Deplacer(i);
    }
    return arr;
}


function triPeigne(arr)
{
 function tableau_trie(arr) {
      var sorted = true;
      for (var i = 0; i < arr.length - 1; i++) {
          if (arr[i] > arr[i + 1]) {
              sorted = false;
              break;
          }
      }
      return sorted;
  }
 
  var iteration_count = 0;
  var ecart = arr.length - 2;
  var decrease_factor = 1.25;
 
  // Repeat iterations Until array is not sorted
  
  while (!tableau_trie(arr)) 
  {
      // If not first ecart  Calculate ecart
      if (iteration_count > 0)
         ecart = (ecart == 1) ? ecart : Math.floor(ecart / decrease_factor);
 
  // Set front and back elements and increment to a ecart
      var front = 0;
      var back = ecart;
      while (back <= arr.length - 1) 
      {
          // Swap the elements if they are not ordered
        
          if (arr[front] > arr[back])
          {
              var temp = arr[front];
              arr[front] = arr[back];
              arr[back] = temp;
          }
 
          // Increment and re-run swapping
        
          front += 1;
          back += 1;
      }
      iteration_count += 1;
  }
  return arr;
}

function triShell(arr) {
	let n = arr.length;

	for (let ecart = Math.floor(n/2); ecart > 0; ecart = Math.floor(ecart/2))	{
		for (let i = ecart; i < n; i += 1)  {
			let temp = arr[i];
			
			let j;
			for (j = i; j >= ecart && arr[j-ecart] > temp; j-=ecart)  {
				arr[j] = arr[j-ecart];
			}

			arr[j] = temp;
		}
	}
    return arr
}

	


module.exports.functionManagement= async (req, res) => {

    const {tableau,typeFunction}=req.body
    if(typeFunction=='triInsertion'){
        var tableauTrie = triParInsertion(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    }else if(typeFunction=='triBulle'){

        var tableauTrie = triBulle(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    }
    else if(typeFunction=='triSelection'){

        var tableauTrie = triParSelection(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } else if(typeFunction=='triFusion'){

        var tableauTrie = triFusion(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } else if(typeFunction=='triRapide'){

        var tableauTrie = triRapide(tableau,0,tableau.length - 1)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } else if(typeFunction=='triShaker'){

        var tableauTrie = triShaker(tableau,tableau.length-2)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } else if(typeFunction=='triTas'){

        var tableauTrie = triParTas(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } else if(typeFunction=='triGnome'){
        var tableauTrie =triGnome(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    }else if(typeFunction=='triPeigne'){
        var tableauTrie =triPeigne(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } else if(typeFunction=='triShell'){
        var tableauTrie =triShell(tableau)
        res.json({status:true,tableau:tableauTrie,typeFunction:typeFunction})
    } 
}
