

const {
    functionRouter
} = require('./router/function.router');
const express = require('express');
const morgan = require('morgan');
const swaggerUI = require('swagger-ui-express');  
const swaggerJSDoc = require('swagger-jsdoc');
var app = express();



app.use(morgan('dev'));
// middleware
app.use(express.json());
app.use(express.urlencoded({
    extended: false
}));

app.use('/api', functionRouter);

//Swagger Configuration  
const swaggerOptions = {  
    swaggerDefinition: {  
        info: {  
            title:'Employee API',  
            version:'1.0.0'  
        }  
    },  
    apis:['server.js'],  
}  
const swaggerDocs = swaggerJSDoc(swaggerOptions);  
app.use('/api-docs',swaggerUI.serve,swaggerUI.setup(swaggerDocs));  


app.listen(5000, () => console.log(`Server started at port :5000`));


