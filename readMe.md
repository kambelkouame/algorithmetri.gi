## setup project

1. git clone 
2. cd algorithmetri
3. npm install 
4. nodemon server.js


[Trié-une-liste]
1. ouvrir postman0
2. Creer une requete en [post]
3. url: localhost:5000/api//tri-recherche
    data:
    {
        "tableau":[1,5,8,9,10,7,6,3,56,89,94],
        "typeFunction:"triInsertion"
    }

    il existe 10 type de function

    1. triInsertion
    2. triBulle
    3. triSelection
    4. triFusion
    5. triRapide
    6. triShaker
    7. triTas
    9. triGnome
    10. triPeigne

    remplacer le typeFunction avec la type de fonction souhaite puis lancer la requete

    vous obtiendrai votre Liste eja trié