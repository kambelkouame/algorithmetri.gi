const express = require('express');
const router = express.Router();

const {functionManagement} = require('../controller/function.controllers');


/** 
 * @swagger 
 * /recherche: 
 *   post: 
 *     description: effectuer un tri avec ube fonction specifique 
 *     parameters: 
 *     - tableau: EmployeeName 
 *       description: Create an new employee 
 *       in: formData 
 *       required: true 
 *       type: String 
 *     responses:  
 *       201: 
 *         description: Created  
 *   
 */  
router.post("/tri-recherche",functionManagement);






//router.post('/update-provider', updateProvider);
module.exports.functionRouter = router;